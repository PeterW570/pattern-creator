# Pattern Creator

UI to pick various regex-like tokens to create a pattern and view matches against test text.

## To run

`npm run serve` when developing

`npm run build` to build for production
